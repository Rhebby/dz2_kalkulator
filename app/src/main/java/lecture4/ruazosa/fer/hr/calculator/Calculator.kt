package lecture4.ruazosa.fer.hr.calculator

import java.nio.charset.CharacterCodingException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.exp

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {

    var result: Double = 0.0
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    fun reset() {
        result = 0.0
        expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "*" -> expression.add(operator)
            "/" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }
    fun applyOp(op:String, b:Double, a:Double): Double{
        when(op){
            "+" -> return a+b
            "-" -> return a-b
            "*" -> return a*b
            "/" -> return a/b
        }
        return 0.0
    }

    fun hasPrecedence(op1:String, op2:String): Boolean{
        if ((op1 == "*" || op1 == "/") && (op2 == "+" || op2 == "-")) {
            return false
        }else {
            return true
        }
    }

    fun evaluate() {

        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }
        result = expression[0].toDouble()
        var vrijednosti : Stack<Double> = Stack()
        var operatori : Stack<String> = Stack()
        var oper : Array<String> = arrayOf("+", "-", "*", "/")
        for(i in 0..expression.count()-1){
            if (oper.contains(expression[i])){
                while(!operatori.isEmpty() && hasPrecedence(expression[i], operatori.peek())){
                    vrijednosti.push(applyOp(operatori.pop(), vrijednosti.pop(), vrijednosti.pop()))
                }
                operatori.push(expression[i])
            }else {
                vrijednosti.push(expression[i].toDouble())
            }
        }
        while(!operatori.isEmpty()){
            vrijednosti.push(applyOp(operatori.pop(), vrijednosti.pop(), vrijednosti.pop()))
        }
        result = vrijednosti.pop()
    }
}